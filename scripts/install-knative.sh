#!/bin/bash

echo N | kn quickstart kind

while [ ! k wait --for=condition=Ready node/knative-control-plane ]
do
    sleep 1
done

echo N | kn quickstart kind
