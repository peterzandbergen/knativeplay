#!/bin/bash

WDIR=$(dirname ${BASH_SOURCE[0]})

kind create cluster --config $WDIR/../kind-config/kind-knative-3-nodes.yaml

while [ ! k wait --for=condition=Ready node/knative-control-plane ]
do
    sleep 1
done


# helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
# helm repo update
# helm upgrade --install --set args={--kubelet-insecure-tls} metrics-server metrics-server/metrics-server --namespace kube-system

# Install using kustomize
kustomize build --enable-helm $WDIR/../kubernetes/metrics-server | kubectl apply -f -