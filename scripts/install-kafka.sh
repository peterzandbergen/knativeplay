#!/bin/bash

WDIR=$(dirname ${BASH_SOURCE[0]})

kustomize build $WDIR/../kubernetes/kafka | kubectl  apply -f -

# kubectl create namespace kafka
# kubectl create -f 'https://strimzi.io/install/latest?namespace=kafka' -n kafka

# Add a wait
while ! kubectl wait kafka/my-cluster --for=condition=Ready --timeout=300s -n kafka
do
    sleep 3
done

# # Install my cluster

# kubectl apply -f https://strimzi.io/examples/latest/kafka/kafka-persistent-single.yaml -n kafka


