# Knative Play

Installation:
- run scripts/install-kuberneters.sh
- run scripts/install-knative.sh
- run scripts/install-kafka.sh

## Links

- [Knative Broker for Kafka][Kafka Broker]
- [Strimzi Kafka Controller][Strimzi]
- [Kustomize Helmchart][Kustomize Helmchart]



[Strimzi]: https://strimzi.io/quickstarts/
[Kafka Broker]: https://knative.dev/docs/eventing/brokers/broker-types/kafka-broker/
[Kustomize Helmchart]: https://kubectl.docs.kubernetes.io/references/kustomize/builtins/#field-name-helmcharts